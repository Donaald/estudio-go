package main

import "fmt"

type Comunicacion interface {
	hablar() string
	escribir() string
}

type Persona struct {
	nombre string
}

type Perro struct {
	nombre string
}

func (persona Persona) hablar() string {
	return "la persona con nombre " + persona.nombre + " puede hablar"
}

func (persona Persona) escribir() string {
	return "la persona con nombre " + persona.nombre + " puede escribir"
}

func (perro Perro) hablar() string {
	return "la perro con nombre " + perro.nombre + " no puede hablar"
}

func (perro Perro) escribir() string {
	return "la perro con nombre " + perro.nombre + " no puede escribir"
}

func main() {

	comunicacion := Persona{nombre: "Donald"}
	fmt.Println(comunicacion.escribir())
	fmt.Println(comunicacion.hablar())

	comunicacion1 := Perro{nombre: "Firulais"}
	fmt.Println(comunicacion1.escribir())
	fmt.Println(comunicacion1.hablar())
}
