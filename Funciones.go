package main

import "fmt"

func main() {

	conParametosSinRetorno("funcion sin retorno")

	a, b := conParametosConVariosRetornos(1, 2)
	fmt.Println("Suma: ", a, "Multiplicacion: ", b)

	a = conParametosConUnRetorno(1, 2)
	fmt.Println("Suma: ", a)
}

func conParametosSinRetorno(texto string) {
	fmt.Println(texto)
}

func conParametosConUnRetorno(a int, b int) int {
	return a + b
}

func conParametosConVariosRetornos(a int, b int) (int, int) {
	return (a + b), (a * b)
}
