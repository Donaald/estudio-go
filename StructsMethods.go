package main

import "fmt"

type Persona struct {
	nombre    string
	edad      int
	direccion Direccion
}

type Direccion struct {
	ciudad       string
	departamento string
}

func (p Persona) agregarNombre(nombre string) Persona {
	p.nombre = nombre
	return p
}

func main() {

	var persona Persona
	persona = Persona{
		nombre: "Donald Armando",
		edad:   26,
		direccion: Direccion{
			ciudad:       "Medellin",
			departamento: "Antioquia"}}
	fmt.Println(persona)
	fmt.Println(persona.agregarNombre("Otro Nombre"))

	direccion := Direccion{ciudad: "Monteria", departamento: "Cordoba"}
	fmt.Println(direccion)

	persona1 := new(Persona)
	persona1.nombre = "Armando Arteaga"
	persona1.edad = 27
	fmt.Println(persona1)

}
