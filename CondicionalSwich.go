package main

import (
	"fmt"
)

func main() {

	var numero int = 5
	if numero < 0 {
		fmt.Println("El numero es negativo")
	} else if numero > 0 {
		fmt.Println("El numero es positivo")
	} else {
		fmt.Println("El numero es cero")
	}

	if numero = 0; numero < 0 {
		fmt.Println("El numero es negativo")
	} else if numero > 0 {
		fmt.Println("El numero es positivo")
	} else {
		fmt.Println("El numero es cero")
	}

	switch numero {
	case 0:
		fmt.Println("El numero es cero")
	default:
		fmt.Println("El numero  no es cero")
	}

	switch numero {
	case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9:
		fmt.Println("El numero se encuentra entre los primeros 10 digitos")
	default:
		fmt.Println("El numero no se encuentra entre los primeros 10 digitos")
	}

	switch {
	case numero < 0:
		fmt.Println("El numero es negativo")
	case numero > 0:
		fmt.Println("El numero es positivo")
	default:
		fmt.Println("El numero es cero")
	}

}
