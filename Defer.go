package main

import "fmt"

func escribir() {
	fmt.Println("funcion escribir")
}

func main() {

	defer escribir()
	fmt.Println("primer fmt")
	fmt.Println("segundo fmt")

}
