package main

import "fmt"

const residencia string = "Medellin - prado"
const direccion = "Estacion hospital"

func main() {

	const edad int = 27
	const diaNacimiento = 20

	const (
		nombreMama    = "Digna del Carmen"
		nombreHermana = "Vianey del carmen"
		nombreAbuela  = "Nacira de la Concepcion"
	)

	var nombre string
	nombre = "Donald Armando"

	apellido := "Torres Arteaga"

	var ciudad = "Monteria"

	var (
		primerNombre    = "Donald"
		segundoNombre   = "Armando"
		primerApellido  = "Torres"
		segundoApellido = "Arteaga"
	)

	fmt.Println(nombre, apellido, ciudad, primerNombre, segundoNombre, primerApellido, segundoApellido)
	fmt.Println(residencia, direccion, edad, diaNacimiento, nombreMama, nombreHermana, nombreAbuela)

}
