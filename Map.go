package main

import "fmt"

func main() {

	mapa := make(map[string]string)

	mapa["nombre"] = "Donald Armando"
	mapa["apellido"] = "Torres Arteaga"
	mapa["ciudad"] = "Monteria"
	mapa["nombre"] = "Armando Donald"

	delete(mapa, "apellido")
	delete(mapa, "Monteria")

	fmt.Println(mapa)

	mapa1 := map[int]int{
		1: 100,
		2: 200,
		3: 300,
	}

	mapa1[2] = 2000

	fmt.Println(mapa1)

	valorClave, estaPresente := mapa1[3]
	fmt.Println(valorClave, estaPresente)

	valorClave, estaPresente = mapa1[4]
	fmt.Println(valorClave, estaPresente)

}
