package main

import "fmt"

func main() {

	fmt.Println("Loop relacion while")
	i := 1
	for i < 10 {
		fmt.Println("valor i ", i)
		i++
	}
	fmt.Println("ultimo valor i ", i)

	fmt.Println("Loop relacion for")
	for j := 0; j < 10; j++ {
		fmt.Println("valor j ", j)
	}

	fmt.Println("Loop 'infinito'")
	for {
		fmt.Println("entro Loop 'infinito'")
		break
	}

	for n := 0; n <= 5; n++ {
		if n%2 == 0 {
			continue
		}
		fmt.Println(n)
	}

}
