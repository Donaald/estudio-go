package main

import "fmt"

//Array = tamaño fijo
//Slice = tamaño dinamico

func main() {

	var arreglo [2]string
	arreglo[0] = "texto arreglo 1"
	arreglo[1] = "texto arreglo 2"
	fmt.Println(arreglo, arreglo[0], arreglo[1])

	arreglo1 := [2]string{"texto arreglo1 1", "texto arreglo1 2"}
	fmt.Println(arreglo1, arreglo1[0], arreglo1[1])

	var slice = make([]int, 3)
	fmt.Println(slice, len(slice))

	slice[0] = 1
	slice[1] = 2
	slice[2] = 3
	fmt.Println(slice, len(slice))

	slice = append(slice, 4)
	slice = append(slice, 5)
	fmt.Println(slice, len(slice))

	var newSlice []int //:= make([]int, len(slice))
	newSlice = append(newSlice, 0, 0, 0, 0, 0)
	copy(newSlice, slice)
	fmt.Println(newSlice)

	newSlice1 := make([]int, len(slice))
	newSlice2 := make([]int, len(slice))
	newSlice3 := make([]int, len(slice))
	newSlice1 = newSlice[2:5]
	newSlice2 = newSlice[:5]
	newSlice3 = newSlice[3:]
	fmt.Println(newSlice1, newSlice2, newSlice3)

}
